// This code has a syntax error AND a logic error.
// This requires external knowledge about how discounts are 
// calculated.
// See if you can fix both errors.
const products = {
    "Shampoo": 10,
    "Conditioner": 8,
    "Soap": 2
};

const product = "Shampoo";
const quantity = 3;

const discount = 0.1;  // A 10% discount
const totalPrice = products[product] * quantity;

if (quantity > 2) {
    totalPrice = totalPrice - discount;
}

console.log("The total price is: $" + totalPrice);
