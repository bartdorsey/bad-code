// This problem has a logic error.
// It doesn't produce a syntax error or a runtime error
// See if you can figure out what's wrong.
const players = ["John", "Sarah", "Alex", "Mary"];
const currentPlayer = players[4];

console.log("The current player is " + currentPlayer);
