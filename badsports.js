const team = "Lakers";
const rankings = {
    "Lakers": 1,
    "Bulls": 5,
    "Celtics": 3
};

const rank = rankings[team];

if rank < 3 {
    console.log("The " + team + " are in the top 3.");
} else {
    console.log("The " + team + " are not in the top 3.");
}
